package br.com.restful.travels;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import br.com.restful.travels.entities.Travel;

public class TravelSpecification {
	
	public static Specification<Travel> search(String text){
		return new Specification<Travel>() {

			private static final long serialVersionUID = 1L;

			@Override
			public Predicate toPredicate(Root<Travel> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				
				if(text == null || text.trim().length() <= 0) return null;
				
				String likeTerm = "%" + text + "%";
				
				Predicate predicate = cb.or(cb.like(root.get("title"), likeTerm));
				
				return predicate;
			}
			
			
		};
		
	}

}