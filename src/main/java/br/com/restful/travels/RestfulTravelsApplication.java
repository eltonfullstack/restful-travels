package br.com.restful.travels;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestfulTravelsApplication {
	
	/*@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}*/

	public static void main(String[] args) {
		SpringApplication.run(RestfulTravelsApplication.class, args);
	}

}
