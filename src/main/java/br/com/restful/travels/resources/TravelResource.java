package br.com.restful.travels.resources;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.restful.travels.dto.TravelDTO;
import br.com.restful.travels.entities.Travel;
import br.com.restful.travels.exception.BadRequestException;
import br.com.restful.travels.models.PageModel;
import br.com.restful.travels.models.PageRequestModel;
import br.com.restful.travels.services.TravelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/travels")
@Api("Restful Travels")
public class TravelResource {
	
	private TravelService travelService;
	
	@Autowired
	public TravelResource(TravelService travelService) {
		this.travelService = travelService;
	}
	
	@GetMapping("/health")
	public String health() {
		return "GOD IS MY POWER EVERY DAY";
	}
	
	@GetMapping
	@ApiOperation("Get all travels")
	public ResponseEntity<PageModel<Travel>> listAll(
			@RequestParam Map<String, String> params){
		
		PageRequestModel pr = new PageRequestModel(params);
		PageModel<Travel> pm = travelService.listAllOnLazyMode(pr);
		
		return ResponseEntity.status(HttpStatus.OK).body(pm);
	}
	
	@GetMapping(value = "/{id}")
	@ApiOperation("Get a travel by id")
	public ResponseEntity<Travel> getTravelById(@PathVariable("id") Long id){
		
		if(!travelService.existsById(id)) {
			throw new BadRequestException("This id not exists");
		}
		
		Travel travel = travelService.getByIdTravel(id);
		return ResponseEntity.status(HttpStatus.OK).body(travel);
		
	}
	
	@PostMapping
	@ApiOperation("Create a travel")
	public ResponseEntity<Travel> createTravel(@RequestBody @Valid TravelDTO dto){
				
		Travel travel = dto.transformToTravel();
		
		if(travelService.existsByTitle(travel.getTitle())) {
			throw new BadRequestException("This title already exists");
		}
		
		Travel createTravel = travelService.saveTravel(travel);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(createTravel);
	}
	
	@PutMapping(value = "/{id}")
	@ApiOperation("Edit a travel by id")
	public ResponseEntity<?> update(@PathVariable("id") Long id, @RequestBody TravelDTO dto){
		
		Travel travel = dto.transformToTravel();
		travel.setId(id);
		
		if(!travelService.existsById(id)) {
			throw new BadRequestException("This id not exists");
		}
		
		travelService.update(travel);		
	
		return ResponseEntity.status(HttpStatus.OK).body("Travel Updated");
	}
	
	@DeleteMapping("/{id}")
	@ApiOperation("Delete a travel by id")
	@ApiResponses({
		@ApiResponse(code = 204, message = "Travel deleted with success")
	})
	public ResponseEntity<?> deteleTravel(@PathVariable("id") Long id){
		
		if(!travelService.existsById(id)) {
			throw new BadRequestException("This id not exists");
		}
		
		travelService.deleteTravelById(id);
		
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Travel deleted.");
		
	}
	

}
