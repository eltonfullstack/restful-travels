package br.com.restful.travels.dto;

public class Mensagens {
	
	private String msg;

	public Mensagens(String msg) {
		this.msg = msg;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
