package br.com.restful.travels.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import br.com.restful.travels.entities.Travel;

public class TravelDTO {
	
	private Long id;
	
	@Size(min = 5, max = 70, message = "The title should be between 5 and 9 characteres")
	private String title;
		
	@Size(max = 100, message="Description should be 100 max characteres")
	private String description;
	
	@NotBlank(message = "Image is required")
	@Size(max = 100, message = "Image should be on max 100 characteres")
	private String image;
	
	public Travel transformToTravel() {
		
		Travel travel = new Travel(null, this.title, this.description, this.image);
		return travel;
		
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}

}
