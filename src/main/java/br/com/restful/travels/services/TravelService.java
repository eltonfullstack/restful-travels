package br.com.restful.travels.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import br.com.restful.travels.TravelSpecification;
import br.com.restful.travels.entities.Travel;
import br.com.restful.travels.exception.NotFoundException;
import br.com.restful.travels.models.PageModel;
import br.com.restful.travels.models.PageRequestModel;
import br.com.restful.travels.repositories.TravelRepository;

@Service
public class TravelService {
	
	private TravelRepository travelRepository;

	@Autowired
	public TravelService(TravelRepository travelRepository) {
		this.travelRepository = travelRepository;
	}
	
	public List<Travel> getAllTravels(){
		
		List<Travel> travels = travelRepository.findAll();
		return travels;
	}
	
	public PageModel<Travel> listAllOnLazyMode(PageRequestModel pr){
		
		Pageable pageable = pr.toSpringPageRequest();
		Specification<Travel> spec = TravelSpecification.search(pr.getSearch());
		
		Page<Travel> page = travelRepository.findAll(spec, pageable);
		
		PageModel<Travel> pm = new PageModel<>((int) page.getTotalElements(), page.getSize(), page.getTotalPages(), page.getContent());
		
		return pm;
	}
	
	public Travel getByIdTravel(Long id) {
		Optional<Travel> result = travelRepository.findById(id);
		return result.orElseThrow(()-> new NotFoundException("There are not travel found with " + id));
	}
	
	public Travel saveTravel(Travel travel) {
		Travel saveTravel = travelRepository.save(travel);
		return saveTravel;
	}
	
	public Travel update(Travel travel) {
		Travel updateTravel = travelRepository.save(travel);
		return updateTravel;
	}
	
	public boolean existsById(Long id) {
		return travelRepository.existsById(id);
	}
	
	public boolean existsByTitle(String title) {
		return travelRepository.existsByTitle(title);
	}
	
	public void deleteTravelById(Long id) {
		travelRepository.deleteById(id);
	}

}
