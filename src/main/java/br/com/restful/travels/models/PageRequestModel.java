package br.com.restful.travels.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

public class PageRequestModel {
	
	private int page = 0;
	private int size = 10;
	private String sort = "";
	private String search = "";
	
	public PageRequestModel(Map<String, String> params) {
		
		if(params.containsKey("pages")) page = Integer.parseInt(params.get("page"));
		if(params.containsKey("size")) size = Integer.parseInt(params.get("size"));
		if(params.containsKey("sort")) sort = params.get("sort");
		if(params.containsKey("search")) search = params.get("search");
		
	}
	
	public PageRequest toSpringPageRequest() {
		
		List<Order> orders = new ArrayList<>();
		String[] properties = sort.split(",");
		
		for(String prop : properties) {
			if(prop.trim().length() > 0) {
				String column = prop.trim();
				
				if(column.startsWith("-")) {
					column = column.replace("-", "");
					orders.add(Order.desc(column));
				}
				else {
					orders.add(Order.asc(column));
				}
			}
		}
		
		return PageRequest.of(page, size, Sort.by(orders));
	}
	
	public PageRequestModel(int page, int size) {
		this.page = page;
		this.size = size;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

}
