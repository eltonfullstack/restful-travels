FROM openjdk:8-alpine
EXPOSE 8080
ADD target/*.jar restful-travels-api.jar
ENTRYPOINT ["sh","-c","java -jar /restful-travels-api.jar"]
